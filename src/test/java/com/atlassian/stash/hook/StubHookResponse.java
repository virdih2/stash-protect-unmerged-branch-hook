package com.atlassian.stash.hook;

import org.hamcrest.Matcher;

import javax.annotation.Nonnull;
import java.io.PrintWriter;
import java.io.StringWriter;

import static org.junit.Assert.*;

public class StubHookResponse implements HookResponse {
    private final StringWriter outStringWriter = new StringWriter();
    private final StringWriter errStringWriter = new StringWriter();

    private final PrintWriter out = new PrintWriter(outStringWriter);
    private final PrintWriter err = new PrintWriter(errStringWriter);

    @Nonnull
    @Override
    public PrintWriter err() {
        return err;
    }

    @Nonnull
    @Override
    public PrintWriter out() {
        return out;
    }

    public void assertStdErr(Matcher<? super String>  matcher) {
        assertThat("StdErr should match", errStringWriter.getBuffer().toString(), matcher);
    }

    public void assertStdOut(Matcher<? super String>  matcher) {
        assertThat("StdOut should match", outStringWriter.getBuffer().toString(), matcher);
    }
}
