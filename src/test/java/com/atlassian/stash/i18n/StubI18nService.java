package com.atlassian.stash.i18n;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.Map;

public class StubI18nService implements I18nService {
    @Override
    public String getText(Locale locale, String key, String fallbackMessage, Object... arguments) {
        return getText(key, fallbackMessage, arguments);
    }

    @Override
    public String getText(String key, String fallbackMessage, Object... arguments) {
        if (arguments != null && arguments.length > 0) {
            return MessageFormat.format(fallbackMessage, arguments);
        } else {
            return fallbackMessage;
        }
    }

    @Override
    public String getMessagePattern(String key, String fallbackPattern) {
        throw new UnsupportedOperationException();
    }

    @Override
    public KeyedMessage getKeyedText(String key, String fallbackMessage, Object... arguments) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Map<String, String> getAllTranslationsForPrefix(String prefix) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Map<String, String> getAllTranslationsForPrefix(String prefix, Locale locale) {
        throw new UnsupportedOperationException();
    }
}
