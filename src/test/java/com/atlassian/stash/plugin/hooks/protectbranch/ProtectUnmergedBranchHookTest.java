package com.atlassian.stash.plugin.hooks.protectbranch;

import com.atlassian.stash.hook.StubHookResponse;
import com.atlassian.stash.hook.repository.RepositoryHookContext;
import com.atlassian.stash.i18n.I18nService;
import com.atlassian.stash.i18n.StubI18nService;
import com.atlassian.stash.nav.NavBuilder;
import com.atlassian.stash.project.Project;
import com.atlassian.stash.project.TestProjectBuilder;
import com.atlassian.stash.pull.*;
import com.atlassian.stash.repository.*;
import com.atlassian.stash.setting.Settings;
import com.atlassian.stash.util.PageRequest;
import com.atlassian.stash.util.PageUtils;
import com.atlassian.stash.util.TestPageUtils;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import static org.mockito.BDDMockito.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verifyNoMoreInteractions;

public class ProtectUnmergedBranchHookTest {

    public static final String TARGET_BRANCH = "target_branch";
    public static final String SOURCE_BRANCH = "refs/heads/feature/source_branch";
    public static final String RANDOM_BRANCH = "refs/heads/random_branch";

    public static final RefChange ADD_RANDOM_BRANCH = new TestRefChangeBuilder().refId(RANDOM_BRANCH).type(RefChangeType.ADD).build();
    public static final RefChange UPDATE_TARGET_BRANCH = new TestRefChangeBuilder().refId(TARGET_BRANCH).type(RefChangeType.UPDATE).build();
    public static final RefChange UPDATE_RANDOM_BRANCH = new TestRefChangeBuilder().refId(RANDOM_BRANCH).type(RefChangeType.UPDATE).build();
    public static final RefChange DELETE_TARGET_BRANCH = new TestRefChangeBuilder().refId(TARGET_BRANCH).type(RefChangeType.DELETE).build();
    public static final RefChange DELETE_SOURCE_BRANCH = new TestRefChangeBuilder().refId(SOURCE_BRANCH).type(RefChangeType.DELETE).build();
    public static final RefChange DELETE_RANDOM_BRANCH = new TestRefChangeBuilder().refId(RANDOM_BRANCH).type(RefChangeType.DELETE).build();

    private static final Project PROJECT = new TestProjectBuilder().key("PROJECT").build();
    private static final Repository REPOSITORY = new TestRepositoryBuilder().slug("REPO").project(PROJECT).build();
    private static final PullRequest TARGET_PULL_REQUEST = new TestPullRequestBuilder().id(1).state(PullRequestState.OPEN).toRef(new TestPullRequestRefBuilder().repository(REPOSITORY).build()).build();
    private static final PullRequest SOURCE_PULL_REQUEST = new TestPullRequestBuilder().id(2).state(PullRequestState.OPEN).toRef(new TestPullRequestRefBuilder().repository(REPOSITORY).build()).build();
    private static final Settings SETTINGS = mock(Settings.class);
    private static final RepositoryHookContext CONTEXT = new RepositoryHookContext(REPOSITORY, SETTINGS);

    private final PullRequestService pullRequestService = mock(PullRequestService.class);
    private final NavBuilder navBuilder = mock(NavBuilder.class, RETURNS_DEEP_STUBS);
    private final I18nService i18nService = new StubI18nService();
    private final StubHookResponse hookResponse = new StubHookResponse();

    private final ProtectUnmergedBranchHook hook = new ProtectUnmergedBranchHook(pullRequestService, i18nService, navBuilder);

    @AfterClass
    public static void verifyNoInteractionsWithSettings() {
        verifyNoMoreInteractions(SETTINGS);
    }

    @Before
    public void setup() {
        // Return an empty page for any find in direction call - we'll override this if needed
        given(pullRequestService.findInDirection(any(PullRequestDirection.class), any(Integer.class), any(String.class), any(PullRequestState.class), any(PullRequestOrder.class), any(PageRequest.class)))
                .willReturn(PageUtils.<PullRequest>createEmptyPage(PageUtils.newRequest(0, 10)));

        // target branch is involved in PR1
        given(pullRequestService.findInDirection(eq(PullRequestDirection.INCOMING), eq(REPOSITORY.getId()), eq(TARGET_BRANCH), eq(PullRequestState.OPEN), any(PullRequestOrder.class), any(PageRequest.class)))
                .willReturn(TestPageUtils.pageContaining(TARGET_PULL_REQUEST));

        // source branch is involved in PR2
        given(pullRequestService.findInDirection(eq(PullRequestDirection.OUTGOING), eq(REPOSITORY.getId()), eq(SOURCE_BRANCH), eq(PullRequestState.OPEN), any(PullRequestOrder.class), any(PageRequest.class)))
                .willReturn(TestPageUtils.pageContaining(SOURCE_PULL_REQUEST));
    }

    @Test
    public void shouldRejectDeletionOfActivePullRequestTargetBranch() throws Exception {
        boolean receiveAllowed = hook.onReceive(CONTEXT, Arrays.asList(DELETE_TARGET_BRANCH), hookResponse);

        assertFalse("Attempt to delete active pull request target branch should not be allowed", receiveAllowed);
    }

    @Test
    public void shouldRejectDeletionOfActivePullRequestSourceBranch() throws Exception {
        boolean receiveAllowed = hook.onReceive(CONTEXT, Arrays.asList(DELETE_SOURCE_BRANCH), hookResponse);

        assertFalse("Attempt to delete active pull request source branch should not be allowed", receiveAllowed);
    }

    @Test
    public void shouldRejectDeletionOfActivePullRequestSourceAndTargetBranches() throws Exception {
        boolean receiveAllowed = hook.onReceive(CONTEXT, Arrays.asList(DELETE_TARGET_BRANCH, DELETE_SOURCE_BRANCH), hookResponse);

        assertFalse("Attempt to delete active pull request source and target branch should not be allowed", receiveAllowed);
    }

    @Test
    public void shouldAllowDeletionOfUnrelatedBranch() throws Exception {
        boolean receiveAllowed = hook.onReceive(CONTEXT, Arrays.asList(DELETE_RANDOM_BRANCH), hookResponse);

        assertTrue("Deleting branches is allowed if they are not connected to active pull requests", receiveAllowed);
    }

    @Test
    public void shouldRejectAnActiveBranchDeleteInterspersedWithOtherChanges() throws Exception {
        boolean receiveAllowed = hook.onReceive(CONTEXT, Arrays.asList(ADD_RANDOM_BRANCH, UPDATE_TARGET_BRANCH, DELETE_TARGET_BRANCH, UPDATE_RANDOM_BRANCH, DELETE_RANDOM_BRANCH), hookResponse);

        assertFalse("Attempt to delete active pull request target branch should not be allowed even if other, uninteresting changes are included", receiveAllowed);
    }

    @Test
    public void shouldAllowChangesToActiveBranch() throws Exception {
        boolean receiveAllowed = hook.onReceive(CONTEXT, Arrays.asList(UPDATE_TARGET_BRANCH), hookResponse);

        assertTrue("Changes to branches with active pull requests are allowed", receiveAllowed);
    }

    @Test
    public void shouldListAllBranchesAndActivePullRequestsOnErr() throws Exception {
        given(navBuilder.repo(REPOSITORY).pullRequest(1).buildAbsolute()).willReturn("http://url.to/project/repo/1");
        given(navBuilder.repo(REPOSITORY).pullRequest(2).buildAbsolute()).willReturn("http://url.to/project/repo/2");

        hook.onReceive(CONTEXT, Arrays.asList(DELETE_TARGET_BRANCH, DELETE_SOURCE_BRANCH), hookResponse);

        // also tests that we strip refs/heads if it exists (on source branch) but don't freak out if it doesn't (on target branch)
        hookResponse.assertStdErr(allOf(containsString("'target_branch'"),
                                        containsString("http://url.to/project/repo/1"),
                                        containsString("'feature/source_branch'"),
                                        containsString("http://url.to/project/repo/2")));
        hookResponse.assertStdOut(is(""));
    }

    @Test
    public void shouldNotSendAnyOutputForAllowedChanges() throws Exception {
        hook.onReceive(CONTEXT, Arrays.asList(ADD_RANDOM_BRANCH, UPDATE_TARGET_BRANCH, UPDATE_RANDOM_BRANCH, DELETE_RANDOM_BRANCH), hookResponse);

        hookResponse.assertStdOut(is(""));
        hookResponse.assertStdErr(is(""));
    }
}
